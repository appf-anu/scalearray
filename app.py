#!/usr/bin/env python

from flask import Flask, Response
from ADCPi import ADCPi
from pprint import pprint as print
import yaml
import os
import re
import time
import json
import datetime
import requests
import traceback

CONFIG_PATH = os.environ.get("SA_CONFIG_PATH", "./config.yaml")
TARGET_URL = os.environ.get("SA_TARGET_URL", "http://telegraf:8080/")


def get_cpu_serial():
    d = ''
    with open('/proc/cpuinfo') as f:
        d = f.read()
    match = re.search(r'^Serial *: (.*)$', d, re.MULTILINE)
    if match is not None:
        return match.group(1)
    return None


app = Flask(__name__)

# Set ADC gain to x4 and sample resolution to 16-bit
adc = ADCPi(0x68, 0x69, 16, bus=1)
adc.set_pga(4)


assert os.path.isfile(CONFIG_PATH), f'config {CONFIG_PATH} does not exist'
config = dict()
with open(CONFIG_PATH) as f:
    config = yaml.load(f.read(), Loader=yaml.SafeLoader)
assert len(config) > 0, f'config {CONFIG_PATH} is empty or not yaml'
interval = config.get('interval', 60)
hub_name = os.environ.get('SA_HUB_NAME', None)
if hub_name is not None:
    assert hub_name in config['hubs'].keys()
    hub = config['hubs'][hub_name]
else:
    cpu_serial = get_cpu_serial() or os.environ.get('SA_CPU_SERIAL', None)
    assert cpu_serial is not None, 'couldnt extract cpu serial from /proc/cpuinfo'
    hubs_bycpuserial = {v['cpu_serial']: {'name': k, **v} for k, v in config['hubs'].items()}
    assert cpu_serial in hubs_bycpuserial.keys(), f'cpu serial {cpu_serial} not found in hubs config section'
    print(f"found {cpu_serial} in config.hubs")
    hub = hubs_bycpuserial[cpu_serial]
    hub_name = hub['name']


def read_temp(temp_sensor_path):
    """
    tries to read temperature from a 1 wire sensor at temp_sensor_path
    tries 10 times, with 0.2s sleep in between.
    if it fails 10 times or cant extract the temp, returns None
    """
    data = ''
    for t in range(10):
        with open(temp_sensor_path, 'r') as f:
            data = f.read()
        correct = data.split('\n')[0].strip()
        if correct[-3:] == 'YES':
            break
        time.sleep(0.2)
    else:
        print(f"too many tries reading from {temp_sensor_path}")
        return None
    match = re.search(r'^.*t=(\d+)$', data, re.MULTILINE)
    if match is not None:
        return float(match.group(1)) / 1000.0
    return None


def get_readings():
    readings = {
        "time": datetime.datetime.now().isoformat(),
        "hub_name": hub_name
    }
    for scale_name, scale_config in hub['scales'].items():
        temp_sensor_id = scale_config['temp_sensor_id']
        temp_sensor_path = f"/sys/bus/w1/devices/{temp_sensor_id}/w1_slave"
        temp = read_temp(temp_sensor_path)
        readings[scale_name] = {"temp": temp}
        adc_channel = scale_config['adc_channel']
        try:
            avg_adc = sum([adc.read_raw(adc_channel) for _ in range(10)]) / 10.0
            readings[scale_name]['avg_adc'] = avg_adc
            v1 = avg_adc - scale_config['zero_load']
            v2 = 5000.0 / (scale_config['5kg_load'] - scale_config['zero_load'])
            v3 = (scale_config['temp_load_cal'] - temp) * scale_config['temp_corr']
            readings[scale_name]['weight'] = (v1 * v2) + v3
        except Exception:
            traceback.print_exc()
            avg_adc = None

    return readings


def get_calibrated_readings(readings):
    # Weight[j] = (((ADC_Raw[j] - cal.ZeroLoad[offset]) * (5000.00 / (cal.Load5Kg[offset] - cal.ZeroLoad[offset]))) +
    # ((cal.TempAtCal[offset] - Temperature[j]) * cal.TempCorr[offset]))

    #
    return None


@app.route("/")
def raw_json():
    try:
        readings = get_readings()
        return Response(json.dumps(readings, indent=4, sort_keys=True),
                        status=200, content_type='application/json')
    except Exception as e:
        print("Exception in main loop")
        traceback.print_exc()
        return Response(json.dumps({"error": str(e)}),
                        status=500, content_type='application/json')


if __name__ == "__main__":
    while True:
        try:
            readings = get_readings()
            for _ in range(3):
                try:
                    resp = requests.post(TARGET_URL, json=readings)
                    if resp.ok:
                        break
                    print(f"request to {TARGET_URL} failed: {resp.reason}")
                except Exception:
                    traceback.print_exc()
            else:
                print("post request ran out of tries, skipping")
        except Exception:
            print("Exception in main loop")
            traceback.print_exc()
