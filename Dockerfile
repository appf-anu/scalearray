FROM alpine:3.14

RUN apk add --no-cache python3 py3-pip py3-smbus git

RUN pip install pyyaml requests flask

RUN python3 -m pip install git+https://github.com/abelectronicsuk/ABElectronics_Python_Libraries.git

COPY app.py .

ENV PYTHONUNBUFFERED=1
ENV FLASK_APP=app

CMD ["flask", "run", "--host=0.0.0.0"]